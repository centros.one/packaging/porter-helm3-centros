package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/centros.one/packaging/porter-helm3-centros/pkg/helm3"
)

var (
	commandFile string
)

func buildInstallCommand(m *helm3.Mixin) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "install",
		Short: "Execute the install functionality of this mixin",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			//Do something here if needed
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return m.Install()
		},
	}
	return cmd
}
