package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/centros.one/packaging/porter-helm3-centros/pkg/helm3"
)

func buildBuildCommand(m *helm3.Mixin) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "build",
		Short: "Generate Dockerfile lines for the bundle invocation image",
		RunE: func(cmd *cobra.Command, args []string) error {
			return m.Build()
		},
	}
	return cmd
}
