module gitlab.com/centros.one/packaging/porter-helm3-centros

go 1.13

require (
	get.porter.sh/porter v0.23.0-beta.1
	github.com/Azure/go-autorest/autorest v0.10.0 // indirect
	github.com/Masterminds/semver v1.5.0
	github.com/PaesslerAG/gval v1.0.1 // indirect
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/ghodss/yaml v1.0.0
	github.com/go-test/deep v1.0.8
	github.com/gobuffalo/packr/v2 v2.8.3
	github.com/googleapis/gnostic v0.5.3 // indirect
	github.com/hashicorp/go-multierror v1.0.0
	github.com/imdario/mergo v0.3.8 // indirect
	github.com/pkg/errors v0.9.1
	github.com/rogpeppe/go-internal v1.8.1 // indirect
	github.com/spf13/cobra v1.4.0
	github.com/stretchr/testify v1.7.0
	github.com/xeipuuv/gojsonpointer v0.0.0-20190905194746-02993c407bfb // indirect
	github.com/xeipuuv/gojsonschema v1.2.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f // indirect
	golang.org/x/sys v0.0.0-20220319134239-a9b59b0215f8 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v2 v2.4.0
	k8s.io/apimachinery v0.19.7
	k8s.io/client-go v0.19.7
)

replace github.com/hashicorp/go-plugin => github.com/carolynvs/go-plugin v1.0.1-acceptstdin
